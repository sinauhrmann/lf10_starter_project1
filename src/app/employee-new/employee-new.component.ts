import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-employee-new',
  templateUrl: './employee-new.component.html',
  styleUrls: ['./employee-new.component.css']
})
export class EmployeeNewComponent implements OnInit {

  @Input() matFormLast: string | undefined = "";
  @Input() matFormFirst: string | undefined = "";
  @Input() matFormStreet: string | undefined = "";
  @Input() matFormPost: string | undefined = "";
  @Input() matFormCity: string | undefined = "";
  @Input() matFormPhone: string | undefined = "";

  constructor() { }

  ngOnInit(): void {
  }

  saveClicked() {
      //employee.lastName = lastName.trim();
      firstName = firstName.trim();
      street = street.trim();
      postcode = postcode.trim();
      city = city.trim();
      phone = phone.trim();
      this.employeeService.addEmployee(this.employee)
      .subscribe(() => this.router.navigateByUrl(''))
  }

}
