import { Component, OnInit } from '@angular/core';

import { Employee } from '../Employee';
import { EmployeeService } from '../employee.service';
import {Router} from "@angular/router";
import {Observable, of} from "rxjs";

@Component({
  selector: 'app-employees',
  templateUrl: './employees.component.html',
  providers: [EmployeeService],
  styleUrls: ['./employees.component.css']
})
export class EmployeesComponent implements OnInit {

  employees$: Observable<Employee[]>;
  employees!: Employee[];

  constructor(private employeeService: EmployeeService, private router: Router) {
      this.employees$ = employeeService.getAllEmployees();
  }

  ngOnInit() {
      this.employeeService.getAllEmployees().subscribe((allEmployees: Employee[]) => {
        this.employees = allEmployees;
     if(this.employees.length == 0) {
       this.employees = this.employeeDummy;
     }});
  }

  getAllEmployees(): void {
    this.employeeService.getAllEmployees()
  }

  employeeClick(clickedEmployee: Employee) {
    localStorage.setItem('clickedId', this.idToString(clickedEmployee.id))
    this.router.navigate(['/employeeDetails']);
  }

  newButtonPressed() {
    this.router.navigate(['/employeeNew']);
  }

  idToString(id: number) {
    let idAsString = "";
    for (let i = 1; i < id; i++) {
      idAsString = idAsString+"a";
    }
    console.log(idAsString.length)
    return idAsString
  }

  employeeDummy: Employee[] = [
    {
      "id": 0,
      "lastName": "Dies ist dummy data",
      "firstName": "Es gab Probleme bei der Request.",
      "street": "",
      "postcode": "",
      "city": "",
      "phone": ""
    },
    {
      "id": 1,
      "lastName": "Dieter",
      "firstName": "Hans",
      "street": "Weg 1",
      "postcode": "28385",
      "city": "Bremen",
      "phone": "8237821"
    },
    {
      "id": 2,
      "lastName": "Klaussen",
      "firstName": "Manfred",
      "street": "Weg 2",
      "postcode": "28385",
      "city": "Bremen",
      "phone": "093427821"
    },
    {
      "id": 3,
      "lastName": "Licht",
      "firstName": "Karen",
      "street": "Weg 3",
      "postcode": "28385",
      "city": "Bremen",
      "phone": "8233245"
    },
    {
      "id": 4,
      "lastName": "Hummer",
      "firstName": "Gerda",
      "street": "Weg 4",
      "postcode": "28385",
      "city": "Bremen",
      "phone": "09312354"
    }
  ];
}
