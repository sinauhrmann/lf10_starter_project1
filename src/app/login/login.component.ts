import {Component, Input, OnInit, Output} from '@angular/core';
import {Router} from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Input() matInputName: string = "";
  @Input() matInputPass: string = "";
  @Output() error: string = "";

  constructor(private router: Router) { }

  ngOnInit(): void {
  }

  goToNextPage() {
    if(this.isNameValid() && this.isPassValid()) {
      this.router.navigate(['/employees']);
    }
  }

  async isUserDataValid() {
    try {
      const response = await fetch('https://authproxy.szut.dev',{
        method:'POST',
        headers:{
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: 'grant_type=password&client_id=employee-management-service&username='+this.matInputName+'&password='+this.matInputPass
      });
      const body = await response.json();
      localStorage.setItem('bToken',JSON.stringify(body.access_token));
      this.error = '';
      this.router.navigate(['/employees']);
    } catch (e) {
      this.error = 'Benutzername oder Passwort sind inkorrekt!';
    }
  }

  isNameValid() {
    return this.matInputName != '';
  }

  isPassValid() {
    return this.matInputPass != '';
  }
}
