import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import { EmployeeDetailComponent } from './employeesDetails/employee-detail.component';
import { EmployeeNewComponent } from './employee-new/employee-new.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatButtonModule} from "@angular/material/button";
import { LoginComponent } from './login/login.component';
import { FormsModule } from '@angular/forms';
import { RouterModule, Routes } from '@angular/router';
import {MatInputModule} from "@angular/material/input";
import { EmployeesComponent } from './employees/employees.component';
import { HttpErrorHandler } from './http-error-handler.service';
import {MatListModule} from "@angular/material/list";
import {EmployeeService} from "./employee.service";

const routes: Routes = [
  { path: '', component: LoginComponent},
  { path: 'employees', component: EmployeesComponent},
  { path: 'employeeDetails', component: EmployeeDetailComponent},
  { path: 'employeeNew', component: EmployeeNewComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    EmployeesComponent,
    AppComponent,
    EmployeeDetailComponent,
    EmployeeNewComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatButtonModule,
    FormsModule,
    RouterModule.forRoot(routes),
    MatInputModule,
    MatListModule
  ],
  exports: [
    AppComponent,
    LoginComponent,
    EmployeesComponent
  ],
  providers: [HttpErrorHandler, EmployeeService],
  bootstrap: [AppComponent]
})
export class AppModule {
}
