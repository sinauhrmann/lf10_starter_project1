import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { HttpHeaders } from '@angular/common/http';

import {Observable, of} from 'rxjs';
import { catchError } from 'rxjs/operators';

import { HttpErrorHandler, HandleError } from './http-error-handler.service';
import {Employee} from "./Employee";

@Injectable()
export class EmployeeService {
  private handleError: HandleError;
  employees$: Observable<Employee[]>;
  bearer: string | null = '';

  constructor(
    private http: HttpClient,
    httpErrorHandler: HttpErrorHandler) {
    this.employees$ = of([]);
    this.handleError = httpErrorHandler.createHandleError('EmployeeService');
      this.bearer = localStorage.getItem('bToken');
  }

  getAllEmployees(): Observable<Employee[]> {
    return this.employees$ = this.http.get<Employee[]>('/backend', {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer}`)
    }).pipe(
        catchError(this.handleError('getAllEmployees', []))
      );
  }

  getEmployee(id: number): Observable<Employee> {
    return this.http.get<Employee>('/backend' + id, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer}`)
    }).pipe(
      catchError(this.handleError<Employee>('getEmployee'))
    );
  }

  addEmployee(employee: Employee): Observable<any> {
    return this.http.post('/backend', employee,{
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer}`)
    }).pipe(
        catchError(this.handleError('addEmployee', employee))
      );
  }

  deleteEmployee(id: number): Observable<Employee[]> {
    return this.employees$ = this.http.delete<Employee[]>('/backend/' + id, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer}`)
    }).pipe(
      catchError(this.handleError<Employee[]>('addEmployee'))
    );
  }

  updateEmployee(employee: Employee): Observable<any> {
    return this.http.put("/backend/"+employee.id, employee, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer}`)
    }).pipe(
      catchError(this.handleError('updateEmployee'))
    )
  }
}
