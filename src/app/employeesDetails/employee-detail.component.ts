import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {Employee} from "../Employee";
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee-detail',
  templateUrl: './employee-detail.component.html',
  styleUrls: ['./employee-detail.component.css']
})
export class EmployeeDetailComponent implements OnInit {

  isButtonVisible: boolean = true;

  employee!: Employee;

  id: number = 0;
  @Input() matFormLast: string | undefined = "";
  @Input() matFormFirst: string | undefined = "";
  @Input() matFormStreet: string | undefined = "";
  @Input() matFormPost: string | undefined = "";
  @Input() matFormCity: string | undefined = "";
  @Input() matFormPhone: string | undefined = "";

  constructor(private employeeService: EmployeeService) {
    console.log(this.employee);
    if (localStorage.getItem('clickedId') !== null) {
      // @ts-ignore
      this.id = localStorage.getItem('clickedId').length;
    }
    let emData;
    this.employeeService.getEmployee(this.id).subscribe(value => this.employee = value);
  }

  ngOnInit(): void {
    this.matFormLast = this.employee.lastName;
    this.matFormFirst = this.employee.lastName;
    this.matFormStreet = this.employee.lastName;
    this.matFormPost = this.employee.lastName;
    this.matFormCity = this.employee.lastName;
    this.matFormPhone = this.employee.lastName;
  }


  editClicked() {
    this.isButtonVisible = false;
  }

  saveClicked() {
    this.isButtonVisible = true;
    this.employeeService.updateEmployee(this.employee);
  }

  deleteClicked() {
    this.employeeService.deleteEmployee(this.id);
  }
}
